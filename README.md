# OpenML dataset: Bank_marketing_data_set_UCI

https://www.openml.org/d/44234

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data is related with direct marketing campaigns of a Portuguese banking institution. The marketing campaigns were based on phone calls. Often, more than one contact to the same client was required, in order to access if the product (bank term deposit) would be ('yes') or not ('no') subscribed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44234) of an [OpenML dataset](https://www.openml.org/d/44234). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44234/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44234/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44234/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

